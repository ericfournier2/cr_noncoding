# This is the main script, which calls the other scripts to perform
# statistical analysis on the RNA-Seq data.

# Load required libraries.
library(reshape)
library(ggplot2)

# Get parameters, either from the command line or from defaults.
parameters <- commandArgs(trailingOnly = TRUE)
if(length(parameters > 0)) {
    organism <- parameters[1]
} else {
    setwd("C:/Dev/Projects/Non-coding transcripts/TranscriptStatistics")
    organism <- "cow"
}

outputDir <- file.path(getwd(), "Output", organism)
inputDir <- file.path(getwd(), "Input", organism)

if(organism=="cow") {
    FPKM_THRESHOLD <- 1e-4 
    REP_THRESHOLD <- 2
} else {
    FPKM_THRESHOLD <- 1e-4 
    REP_THRESHOLD <- 1
}                    

# Call sub-scripts.
source("Scripts/LoadData.R")
source("Scripts/CategorizeTranscripts.R")
source("Scripts/GenerateTable1.R")
source("Scripts/PlotCategoryProfiles.R")
source("Scripts/PlotPaperProfileFigures.R")

#categorySummary <- c(All                = sum(!excludedTranscripts),
#                     RibosomeAssociated = sum(ribosomeAssociated & !excludedTranscripts),
#                     CodingLoci         = sum(codingLocus        & !excludedTranscripts),
#                     CodingTranscripts  = sum(codingTranscript   & !excludedTranscripts),
#                     Spliced            = sum(spliced            & !excludedTranscripts),
#                     TransposonAny      = sum(transposonAny      & !excludedTranscripts),
#                     TransposonLike     = sum(transposonLike     & !excludedTranscripts),
#                     TransposonFull     = sum(transposonFull     & !excludedTranscripts),
#                     KnownLoci          = sum(knownLocus         & !excludedTranscripts),
#                     KnownTranscripts   = sum(knownTranscript    & !excludedTranscripts),
#                     KnownIsoforms      = sum(knownIsoforms      & !excludedTranscripts),
#                     NewIsoforms        = sum(newIsoforms        & !excludedTranscripts),
#                     Mitochondrial      = sum(mitochondrial      & !excludedTranscripts))
#                     
#catDF <- data.frame(Number=categorySummary, Proportion=categorySummary/categorySummary[1])    
#
#locusSummary    <- c(All                = length(unique(annotation$Locus[!excludedTranscripts])),
#                     RibosomeAssociated = length(unique(annotation$Locus[ribosomeAssociated & !excludedTranscripts])),
#                     CodingLoci         = length(unique(annotation$Locus[codingLocus        & !excludedTranscripts])),
#                     CodingTranscripts  = length(unique(annotation$Locus[codingTranscript   & !excludedTranscripts])),
#                     Spliced            = length(unique(annotation$Locus[spliced            & !excludedTranscripts])),
#                     TransposonAny      = length(unique(annotation$Locus[transposonAny      & !excludedTranscripts])),
#                     TransposonLike     = length(unique(annotation$Locus[transposonLike     & !excludedTranscripts])),
#                     TransposonFull     = length(unique(annotation$Locus[transposonFull     & !excludedTranscripts])),
#                     KnownLoci          = length(unique(annotation$Locus[knownLocus         & !excludedTranscripts])),
#                     KnownTranscripts   = length(unique(annotation$Locus[knownTranscript    & !excludedTranscripts])),
#                     KnownIsoforms      = length(unique(annotation$Locus[knownIsoforms      & !excludedTranscripts])),
#                     NewIsoforms        = length(unique(annotation$Locus[newIsoforms        & !excludedTranscripts])),
#                     Mitochondrial      = length(unique(annotation$Locus[mitochondrial      & !excludedTranscripts])))
#                     
#locusDF <- data.frame(Number=locusSummary, Proportion=locusSummary/locusSummary[1])       




                               
###### Miscellaneous plots #######

# Comparison of the lengths of coding vs non-coding transcripts.
lengthDensityDF <- data.frame(Length=annotation$Transcript_Length[!excludedTranscripts],
                              Coding=annotation$CodingTranscript[!excludedTranscripts])
ggplot(lengthDensityDF, aes(x=Length, fill=Coding)) +
    geom_density(alpha=0.3) +
    xlim(0, 12500) +
    labs(x="Transcript Length", title="Density plot of transcript length\nfor coding and non-coding transcripts")
ggsave(file.path(outputDir, "Density plot of transcript length for coding and non-coding transcripts.tiff"), width=7, height=7, dpi=600)

# Density plot of FPKM for coding vs non-coding
# Plot mean FPKM across conditions
fpkmDensityDF <- data.frame(FPKM=log10(apply(annotation[!excludedTranscripts,countColumns], 1, sum, na.rm=TRUE)),
                            Coding=annotation$CodingTranscript[!excludedTranscripts])
ggplot(fpkmDensityDF, aes(x=FPKM, fill=Coding)) +
    geom_density(alpha=0.3) +
    xlim(-7.5, 1.5) +
    labs(x="log10(FPKM)", title="Density plot of mean FPKM across conditions\nfor coding and non-coding transcripts")
ggsave(file.path(outputDir, "Density plot of mean FPKM across conditions for coding and non-coding transcripts.tiff"), width=7, height=7, dpi=600)

# Density plots of the number of replicates
replicateDensityDF <- data.frame(Replicates=apply(annotation[!excludedTranscripts,countColumns]>0, 1, sum, na.rm=TRUE),
                            Coding=annotation$CodingTranscript[!excludedTranscripts])
ggplot(replicateDensityDF, aes(x=Replicates, fill=Coding)) +
#    geom_density(alpha=0.3) +
    geom_bar(position="dodge", stat="bin") +
#    xlim(-7.5, 1.5) +
    labs(x="log10(FPKM)", title="Histogram of the number of replicates\nin which a transcript is found\nfor coding and non-coding transcripts")
ggsave(file.path(outputDir, "Histogram of number of replicates for coding and non-coding transcripts.tiff"), width=7, height=7, dpi=600)
plotRelativeToFPKM(apply(annotation[,countColumns]>0, 1, sum, na.rm=TRUE), "Number of replicates", "Number of replicates")

# Plot individual FPKMs, per condition.
individualFPKMs <- melt(data.frame(log10(annotation[!excludedTranscripts,countColumns]), Coding=annotation$CodingTranscript[!excludedTranscripts]), id.vars=Coding)
ggplot(individualFPKMs, aes(x=value, fill=Coding)) +
    geom_density(alpha=0.3) +
    xlim(-50, 2) +
    facet_wrap(~variable, ncol=3, nrow=10) +
    labs(x="log10(FPKM)", title="Per condition density plot of FPKM\nfor coding and non-coding transcripts")
ggsave(file.path(outputDir, "FPKM Density All.png"), width=14, height=21)
    

    
# Relationship between total FPKM and coding potential.
plotRelativeToFPKM <- function(yValues, yLabel, plotTitle) {
    fpkmSums <- apply(annotation[!excludedTranscripts,countColumns], 1, sum, na.rm=TRUE)
    
    orderedSums <- sort(fpkmSums)
    orderedYValues <- yValues[!excludedTranscripts][order(fpkmSums)]
    
    i <- 1
    results <- data.frame(FPKM=c(), Value=c())
    while(i < length(fpkmSums)) {
        uLimit <- min(i + 100, length(fpkmSums))
        meanY <- mean(orderedYValues[i:uLimit], na.rm=TRUE)
        fpkm <- mean(orderedSums[i:uLimit], na.rm=TRUE)
        results <- rbind(results, data.frame(FPKM=log10(fpkm), Value=meanY))
        i <- i + 100
    }
    
    ggplot(results, aes(x=FPKM, y=Value)) +
        geom_point(alpha=0.2) +
        xlim(-15, 1) +
        labs(title=plotTitle, x="log10(FPKM)", y=yLabel)
        
    ggsave(file.path(outputDir, paste(plotTitle, ".tiff", sep="")), width=7, height=7, dpi=600, compression="lzw")
}

# Relationship between total FPKM and coding potential.
plotRelativeToFPKM2 <- function(yValues, yLabel, plotTitle) {
    results <- data.frame(FPKM=c(), Value=c(), Tissue=c())
    loessResults <- data.frame(FPKM=c(), Value=c(), Tissue=c())
    for(tissueName in names(columnsByTissue)) {
        fpkmSums <- apply(annotation[!excludedTranscripts,columnsByTissue[[tissueName]] ], 1, mean, na.rm=TRUE)
        
        orderedSums <- sort(fpkmSums)
        orderedYValues <- yValues[!excludedTranscripts][order(fpkmSums)]
        
        i <- 1
        tissueResults <- data.frame(FPKM=c(), Value=c(), Tissue=c())
        while(i < length(fpkmSums)) {
            uLimit <- min(i + 100, length(fpkmSums))
            meanY <- mean(orderedYValues[i:uLimit], na.rm=TRUE)
            fpkm <- mean(orderedSums[i:uLimit], na.rm=TRUE)
            tissueResults <- rbind(tissueResults, data.frame(FPKM=log10(fpkm), Value=meanY, Tissue=tissueName))
            i <- i + 100
        }

        results <- rbind(results, tissueResults)
        tissueResults <- tissueResults[!is.infinite(tissueResults$FPKM),]
        
        loessObj <- loess(tissueResults$Value ~ tissueResults$FPKM, degree=2, span=0.25)
        loessPred <- predict(loessObj, seq(-15, 1, by=0.2))  
        loessPredDF <- data.frame(Loess=loessPred, FPKM=seq(-15, 1, by=0.2))
        loessResults <- rbind(loessResults, data.frame(FPKM=seq(-15, 1, by=0.2), Value=loessPred, Tissue=tissueName))

    }
    results$Tissue <- factor(results$Tissue)
    loessResults$Tissue <- factor(loessResults$Tissue)
    
    ggplot(loessResults, aes(x=FPKM, y=Value, color=Tissue, group=Tissue)) +
        geom_line(alpha=0.2) +
        xlim(-15, 1) +
        labs(title=plotTitle, x="log10(FPKM)", y=yLabel)
        
    ggsave(file.path(outputDir, paste(plotTitle, "2.tiff", sep="")), width=7, height=7, dpi=600, compression="lzw")
}
plotRelativeToFPKM2(annotation$Transposon_Percent, "Transposon Percent", "Transposon percent as a function of FPKM")


plotRelativeToFPKM(annotation$CodingTranscript=="Coding", "Percentage of coding transcripts", "Percentage of coding transcripts as a function of FPKM")
plotRelativeToFPKM(annotation$Transcript_Length, "Transcript Length", "Transcript length as a function of FPKM")
plotRelativeToFPKM(annotation$Transposon_Percent, "Transposon Percent", "Transposon percent as a function of FPKM")
plotRelativeToFPKM(annotation$Number_Of_Exons, "Number of exons", "Number of exons as a function of FPKM")
                       
# Coverage tests
#coverageRaw <- read.table("C:/Users/erfou27/Desktop/All.FPKM.nolength", sep="\t", header=TRUE, quote="")
#coverageSubset <- coverageRaw[!grepl("ERCC|Un", coverageRaw$locus),c(1, which(substr(colnames(coverageRaw), 1, 8)=="coverage"))]
## Validate that rows match
## sum(as.character(coverageSubset[,1])!=as.character(annotation$Transcript))
#coverageFPKM <- cbind(unlist(coverageSubset[,-1], use.names=FALSE), unlist(annotation[,countColumns], use.names=FALSE))
#plot(y=log10(coverageFPKM[,2]), x=log10(coverageFPKM[,1]))
#plotRelativeToFPKM(apply(coverageSubset[,-1], 1, sum), "Coverage", "Coverage")
                               
# Generate plots for the most expressed 8c_tot transcripts to try to determine
# which ones cause the spike in expression/variability.
nonCodingAnnotNRL <- annotation[nonCoding & !ribosomeLikeRNAs,]
ncNRLSum <- apply(nonCodingAnnotNRL[,c(28:30)], 1, sum)
transcriptsOfInterest <- nonCodingAnnotNRL[order(ncNRLSum, decreasing=TRUE),]$Transcript[1:30]
for(i in 1:length(transcriptsOfInterest)) {
    plotExpressionProfile(annotation[,countColumns], annotation$Transcript == transcriptsOfInterest[i], tissue, file.path(outputDir, "TOI"), transcriptsOfInterest[i])
}

distBins <- cut(annotation$Absolute_Distance, breaks=c(-1, 0, seq(1000, 3000000, 1000)))
sumExpr <- apply(annotation[,countColumns], 1, sum)
dataToPlot <- data.frame(Distance=distBins, Expression=sumExpr)[annotation$Class_Code == "-" & !excludedTranscripts,]
test <- aggregate(dataToPlot$Expression, list(Distance=dataToPlot$Distance), mean)
test2 <- test
test2$x[test2$x != 0] <- log10(test2$x)
ggplot(test2, aes(x=Distance, y=x)) + geom_point()
cor(as.integer(test2$Distance), test2$x)




# ERV
ervList <- scan("cow/ERV.list", what=character())
erv <- annotation$Transcript %in% ervList
ervDF               <- plotProfile(erv, "ERV", excluded=excludedTranscripts)

ervFull <- erv & transposonFull
ervLike <- erv & transposonLike
plotProfile(ervFull, "ERVFull", excluded=excludedTranscripts)
plotProfile(ervLike, "ERVLike", excluded=excludedTranscripts)

ervPerc <- read.table("cow/ERV.perc.out", header=FALSE, sep="\t")