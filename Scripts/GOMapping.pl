#! /usr/bin/perl

# Creates a mapping between GO term IDs and probe names, and prints it to
# standard output.

use ReadTable;
my @GOColNames = ("DB", "DBObjectID", "Symbol", "Qualifier", "GOID", "GOReference", "EvidenceCode", "WithOrFrom", "Aspect", "GeneName", "Synonym", "ObjectType", "Taxon", "Date", "AssignedBy", "Extension", "GeneProductFormID");
my $goAnnot = readTableUsingNames(\@GOColNames, "Input/cow/gene_association.goa_cow", "\t", "Symbol", "GOID");

my $rnaAnnot = readTable("Input/cow/Annotations.txt.nosequence.txt", "\t", "Gene");

foreach my $gene (keys %{$rnaAnnot}) {
    if(exists($goAnnot->{$gene})) {
        print $gene;
        my $firstOnLine = 1;
        foreach my $goid (keys %{$goAnnot->{$gene}}) {
            if($firstOnLine) {
                print "\t"  . $goid;
            $firstOnLine = 0;
            } else {
                print ", " . $goid;
            }
        }
        print "\n";
    }
}