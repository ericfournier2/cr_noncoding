cloneAnnotations <- read.table("Input/cow/CloneAnnotations.txt", sep="\t", header=TRUE, quote="")

for(cloneID in unique(cloneAnnotations$Clone)) {
    transcriptIndices <- annotation$Transcript %in% cloneAnnotations$Transcript[cloneAnnotations$Clone==cloneID]
                 
    plotProfile(transcriptIndices, cloneID, excluded=excludedTranscripts, subdir="Old NTRs", writeAnnotation=TRUE,
                               plotNumbers=FALSE,
                               pointPlotOnly=TRUE,
                               plotIsoforms=FALSE,
                               generateStats=FALSE)
}
